package com.yeamgood.springboot_database.repository;

import com.yeamgood.springboot_database.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {

}
