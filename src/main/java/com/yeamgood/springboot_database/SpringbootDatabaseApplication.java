package com.yeamgood.springboot_database;

import com.yeamgood.springboot_database.model.Student;
import com.yeamgood.springboot_database.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDatabaseApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(StudentRepository studentRepository){
		return args -> {
			Student yeam = new Student(
					"yeam",
					"good",
					"yeamgood@gmail.com",
					34
			);
			studentRepository.save(yeam);
			Student boy = new Student(
					"boy",
					"data",
					"boy@gmail.com",
					35
			);
			studentRepository.save(boy);
		};
	}
}
